#ifndef ROC_PACKET_H
#define ROC_PACKET_H

#include <vector>
#include <string>
#include <cstdint>

#include <stdint.h>
#include <TMath.h>

class ROC_Packet
{

    public :
        ROC_Packet();
        virtual ~ROC_Packet(){};

        bool isNull() { return m_isNull; }

        uint32_t SOP() { return m_SOP; }
        uint32_t ROCID() { return m_ROCID; }

        bool withTDC() { return m_withTDC; }

        uint32_t orbit() { return m_orbit; }
	uint32_t BCID() { return m_BCID; }
        uint32_t L1ID() { return m_L1ID; }

        uint32_t nhits() { return m_nhits; }
	std::vector<uint32_t> hit_isDummy()  { return m_hit_isDummy; }
	std::vector<uint32_t> hit_neighbour(){ return m_hit_neighbour; }
	std::vector<uint32_t> hit_parity()   { return m_hit_parity; }
	std::vector<uint32_t> hit_rel_BCID() { return m_hit_rel_BCID; }
	std::vector<uint32_t> hit_vmm_id()   { return m_hit_vmm_id; }
	std::vector<uint32_t> hit_chan_id()  { return m_hit_chan_id; }
	std::vector<uint32_t> hit_ADC()      { return m_hit_ADC; }
	std::vector<uint32_t> hit_TDC()      { return m_hit_TDC; }

        uint32_t error_flag() { return m_error_flag; }
        uint32_t timeout_flag() { return m_timeout_flag; }
        uint32_t VMM_missing_data_flags() { return m_VMM_missing_data_flags; }
        uint32_t L0ID() { return m_L0ID; }

	uint32_t elink_id() { return m_elink_id; }
        uint32_t length() { return m_length; }

        uint32_t checksum() { return m_checksum; }
        uint32_t EOP() { return m_EOP; }
 
	//---------------------------------//
	
        void set_isNull(bool isNull)   { m_isNull = isNull; }

	void set_elink_id( uint32_t elink_id ) { m_elink_id = elink_id; }

        void set_SOP(  uint32_t SOP)   { m_SOP = SOP; }
        void set_ROCID(uint32_t ROCID) { m_ROCID=ROCID; }

        void set_withTDC(bool withTDC) { m_withTDC = withTDC; }

        void set_orbit( uint32_t orbit) { m_orbit = orbit; }
        void set_BCID(uint32_t BCID) { m_BCID = BCID; }
        void set_L1ID(uint32_t L1ID) { m_L1ID = L1ID; }

        void set_nhits(uint32_t nhits) { m_nhits = nhits; }
	void set_hit_isDummy( std::vector<uint32_t> hit_isDummy)  { m_hit_isDummy  = hit_isDummy; }
	void set_hit_parity(  std::vector<uint32_t> hit_parity)   { m_hit_parity   = hit_parity; }
	void set_hit_neighbour( std::vector<uint32_t> hit_neighbour ) { m_hit_neighbour = hit_neighbour; }
	void set_hit_rel_BCID(std::vector<uint32_t> hit_rel_BCID) { m_hit_rel_BCID = hit_rel_BCID; }
	void set_hit_vmm_id(  std::vector<uint32_t> hit_vmm_id)   { m_hit_vmm_id   = hit_vmm_id; }
	void set_hit_chan_id( std::vector<uint32_t> hit_chan_id)  { m_hit_chan_id  = hit_chan_id; }
	void set_hit_ADC(     std::vector<uint32_t> hit_ADC)      { m_hit_ADC      = hit_ADC; }
	void set_hit_TDC(     std::vector<uint32_t> hit_TDC)      { m_hit_TDC      = hit_TDC; }

        void set_error_flag(uint32_t error_flag)            { m_error_flag = error_flag; }
        void set_timeout_flag(uint32_t timeout_flag)        { m_timeout_flag = timeout_flag; }
        void set_VMM_missing_data_flags(uint32_t data_flag) { m_VMM_missing_data_flags = data_flag; }
        void set_L0ID(uint32_t L0ID)                        { m_L0ID = L0ID; }
        void set_length(uint32_t length)                    { m_length = length; }

        void set_checksum(uint32_t checksum)                { m_checksum =  checksum; }
	void set_EOP(uint32_t EOP)                          { m_EOP = EOP; }

	void clear();

    private :

	bool m_isNull;

	uint32_t m_elink_id;
	
	uint32_t m_SOP;
	uint32_t m_ROCID;

	bool m_withTDC;

	uint32_t m_orbit;
	uint32_t m_BCID;
	uint32_t m_L1ID;

	uint32_t m_nhits;
	std::vector<uint32_t> m_hit_isDummy;
	std::vector<uint32_t> m_hit_parity;
	std::vector<uint32_t> m_hit_neighbour;
	std::vector<uint32_t> m_hit_rel_BCID;
	std::vector<uint32_t> m_hit_vmm_id;
	std::vector<uint32_t> m_hit_chan_id;
	std::vector<uint32_t> m_hit_ADC;
	std::vector<uint32_t> m_hit_TDC;

	uint32_t m_error_flag;
	uint32_t m_timeout_flag;
	uint32_t m_VMM_missing_data_flags;
	uint32_t m_L0ID;
	uint32_t m_length;

	uint32_t m_checksum;
	uint32_t m_EOP;

}; // class


#endif

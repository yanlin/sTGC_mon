#include "roc_packet.h"

ROC_Packet::ROC_Packet() :
  m_isNull(false),
  m_SOP(0),
  m_ROCID(0),
  m_withTDC(false),
  m_orbit(0),
  m_BCID(0),
  m_L1ID(0),
  m_nhits(0),
  m_error_flag(0),
  m_timeout_flag(0),
  m_VMM_missing_data_flags(0),
  m_L0ID(0),
  m_elink_id(0),
  m_length(0),
  m_checksum(0)
{
  m_hit_isDummy.resize(0);

  m_hit_parity.resize(0);
  m_hit_neighbour.resize(0);
  m_hit_rel_BCID.resize(0);
  m_hit_vmm_id.resize(0);
  m_hit_chan_id.resize(0);
  m_hit_ADC.resize(0);
  m_hit_TDC.resize(0);
}

void ROC_Packet::clear()
{
  m_hit_isDummy.resize(0);

  m_hit_parity.resize(0);
  m_hit_neighbour.resize(0);
  m_hit_rel_BCID.resize(0);
  m_hit_vmm_id.resize(0);
  m_hit_chan_id.resize(0);
  m_hit_ADC.resize(0);
  m_hit_TDC.resize(0);

  m_isNull = false;
  m_SOP = 0;
  m_ROCID = 0;
  m_withTDC = false;
  m_orbit = 0;
  m_BCID = 0;
  m_L1ID = 0;
  m_nhits = 0;
  m_error_flag = 0;
  m_timeout_flag = 0;
  m_VMM_missing_data_flags = 0;
  m_L0ID = 0;
  m_elink_id = 0;
  m_length = 0;
  m_checksum = 0;

}

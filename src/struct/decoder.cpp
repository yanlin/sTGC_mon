#include "decoder.h"

#include "reply_type.h"

// std/stl
#include <iostream>
#include <sstream>
#include <stdio.h>
#include <iomanip>
using namespace std;

// boost

///////////////////////////////////////////////////////////////////////////////
Decoder::Decoder() :
  m_dbg(false),
    listen(false)
{
  m_ROC_data_packet = new ROC_Packet();
  clear();
}
///////////////////////////////////////////////////////////////////////////////
void Decoder::clear()
{

  m_current_status.resize(0);
  n_replies = 0;
  listen = false;

  m_ROC_data_packet->clear();

  sent_SCA_ADC = false;
  sent_SCA_init = false;
  sent_SCA_GPIO_readbk = false;

  m_tdo.resize(0);
  m_pdo.resize(0);
  m_chan.resize(0);
  m_vmm_id.resize(0);
  m_bcid_rel.resize(0);
  m_flag.resize(0);
  
  m_board_id.resize(0);
  m_bcid.resize(0);
  m_l1_id.resize(0);
  
  m_locked_elink.resize(0);
  
  m_raw_bcid = 0;
  m_raw_l1id = 0;
  
  m_raw_bcid = 0;
  m_raw_vmm_ID = 0;
  m_raw_chan_ID = 0;
  m_raw_pdo = 0;
  m_raw_tdo = 0;
  
  m_TTC_reg_address.resize(0);
  m_TTC_reg_value.resize(0);
  
  m_current_SCA_ID = 0;
  m_current_SCA_ADC.resize(0);
  m_current_SCA_GPIO_mask = 0;

  m_current_EVID = 0;
  m_current_TTC_status = 0;
  m_current_firmware_version = 0;

  m_avg_SCA_ADC = 0;
  
}
uint32_t Decoder::reverse_bits( uint32_t orig, int size){
  
  uint32_t reversed = 0;

  uint32_t bit_mask = 0b1;
  for ( int i=0; i<size; i++ ) {

    uint32_t tmp = ( orig & bit_mask ) >> i;
    reversed += ( tmp << ( size-1 - i ) );

    bit_mask = bit_mask << 1;
  }

  return reversed;
}

//----------------------------------------------------------------------//

unsigned int Decoder::grayToBinary(unsigned int num)
{
  num = num ^ (num >> 16);
  num = num ^ (num >> 8);
  num = num ^ (num >> 4);
  num = num ^ (num >> 2);
  num = num ^ (num >> 1);
  return num;
}

/*
uint32_t Decoder::grayToBinary(uint32_t gray_num)
{
  uint32_t mask;
  for (mask = gray_num >> 1; mask != 0; mask = mask >> 1)
    {
      gray_num = gray_num ^ mask;
    }
  return gray_num;

}
*/
//----------------------------------------------------------------------//
bool Decoder::decode_reply(std::vector<uint32_t> datagram, uint32_t &udp_id, 
			      int &message_type, std::string *out_str, bool decode_data)
{

  stringstream m_sx;

  bool current_status;

  // different reply messages                                            
  //-------------------------------------------------------------------// 
  //   /header (16 bits)/UDP_ID (16 bits)/Module ID (8 bits)              
  //           /Message Type (8 bits)/0x0000 (16 bit place holder)
  //           /0x0 (1 bit place holder) + Data (31 bits)         
  //                   (perhaps multiple 32 bit place holder+data packets)
  //           /0xFFFFFFFF (32 bit trailer)                               
  //-------------------------------------------------------------------// 
  //   m_buffer is a string of hexadecimals                               
  //-------------------------------------------------------------------// 


  if ( datagram.size() < 3 ) {
    m_sx.str("");
    m_sx << "ERROR: reply size is too small. isize " << datagram.size() << " \n";
    *out_str = m_sx.str();
    current_status = false;
    return current_status;
  }

  //------------------------------------------------------------------//
  //                 Break Up Message into Parts                        
  //------------------------------------------------------------------//

  // first 32 bits 
  uint32_t header_hex = ( datagram.at(0) & 0xffff0000 ) >> 16;
  uint32_t UDP_ID_hex = ( datagram.at(0) & 0x0000ffff ) ;

  // second 32 bits
  uint32_t Mod_ID_hex       = ( datagram.at(1) & 0xff000000 ) >> 24;
  uint32_t Message_Type_hex = ( datagram.at(1) & 0x00ff0000 ) >> 16;
  uint32_t Message_ID_hex   = ( datagram.at(1) & 0x0000ff00 ) >> 8;
  //  uint32_t UnDef_hex        = ( datagram.at(1) & 0x000000ff ) ;

  uint32_t trailer_hex      = datagram.back();
  uint32_t next_to_last_hex  = 0;
  if ( datagram.size() >= 2 ) {
    next_to_last_hex = datagram.at( datagram.size()-2 ); // used to decode raw packages
  }

  //-----------------------------------------------------------------//
  //               Check Header + Trailer                              
  //-----------------------------------------------------------------//

  udp_id = UDP_ID_hex;

  outputDatagram(datagram);

  if ( header_hex != 0xface ) {

    if ( ( next_to_last_hex & 0xfffffff0 ) == 0x77700ce0 ) {
      message_type = raw_data; 
      if ( m_dbg ) std::cout << "raw data message" << std::endl;
      current_status = true;
    }
    else {
      *out_str = "ERROR: reply message header is not 0xface \n";
      outputDatagram(header_hex);
      if ( m_dbg ) std::cout <<"unrecognized message: header not 0xface" << std::endl;
      message_type = error_formatting;
      current_status = false;
    }
  }

  else if ( ( next_to_last_hex & 0xfffffff0 ) == 0x77700ce0 ) {
    message_type = raw_data;
    if ( m_dbg ) std::cout <<"raw data message" << std::endl;
    current_status = true;
  }

  else if ( trailer_hex != 0xffffffff )  {
    *out_str = "ERROR: reply message trailer is not 0xffffffff \n";
    outputDatagram(trailer_hex);
    message_type = error_formatting;
    if ( m_dbg ) std::cout <<"unrecognized message: trailer not 0xffffffff" << std::endl;
    current_status = false;
  }

  //---------------------------------------------------------------//             
  //             Decode Module ID, Message Type                              
  //---------------------------------------------------------------//     

  //--------------------------------------------------------------//       
  //                         ROC data                                     
  //--------------------------------------------------------------//       

  else if ( Mod_ID_hex == rep_modID_ROCout ) {

    if ( Message_Type_hex == 0x02 && Message_ID_hex == 0x00 ) {
      message_type = ROC_data;
      if ( m_dbg ) decode_ROC_asic_packet( datagram );
      current_status = true;
    }
    else if ( Message_Type_hex == 0xEE && Message_ID_hex == 0xFF ) {
      message_type = elink_FIFO_full;
      if ( m_dbg ) std::cout <<"error packet: elink fifo full" << std::endl;
      current_status = false;
    }
    else {
      message_type = ROC_unknown_error;
      if ( m_dbg ) std::cout <<"error packet: unknown ROC error" << std::endl;
      current_status = false;
    }

  }

  //---------------------------------------------------------------//
  //                  Ethernet receive module
  //---------------------------------------------------------------//

  else if ( Mod_ID_hex == rep_modID_ethernet_rec ) {
    if      ( Message_Type_hex == rep_type_echo ) {
      message_type = ethernet_rec_echo;
      *out_str = "echo message \n";
      if ( m_dbg ) std::cout <<"info packet: echo message" << std::endl;
	// outputDatagram(datagram);
      current_status = true;
    }
    else if ( Message_Type_hex == rep_type_err ) {
      message_type = ethernet_rec_err_unknown_cmd;
      if ( datagram.at(2) == 0x0bad0000 ) {
        *out_str = "Error: unknown command \n";
	if ( m_dbg ) std::cout <<"error packet: ethernet receiver unrecognized command" << std::endl;
	// outputDatagram(datagram);
	current_status = false;
      }
      else if ( datagram.at(2) == 0x0bad0001) {
	message_type = ethernet_rec_err_FIFO_full;
        *out_str = "Error: Ethernet receiving FIFO full \n";
        if ( m_dbg ) std::cout <<"error packet: ethernet receiver fifo full" << std::endl;
	// outputDatagram(datagram);
	current_status = false;
      }
      else if ( datagram.at(2) == 0x0bad0002) {
	message_type = ethernet_rec_err_target_timeout;
        *out_str = "Error: timeout waiting for target module \n";
        if ( m_dbg ) std::cout <<"error packet: ethernet receiver target timeout" << std::endl;
	// outputDatagram(datagram);
	current_status = false;
      }
      else {
	message_type = ethernet_rec_err_undef;
	*out_str = "Error: Ethernet Receiver Unknown Error Type \n";
        if ( m_dbg ) std::cout <<"error packet: ethernet receiver unknown error" << std::endl;
	// outputDatagram(datagram);
	current_status = false;
      }
    }
    else {
      message_type = ethernet_rec_err_undef;
      *out_str = "Error: Ethernet Receiver Unknown Message Type \n";
      if ( m_dbg ) std::cout <<"error packet: ethernet receiver unknown message type" << std::endl;
      // outputDatagram(datagram);
      current_status = false;
    }

  }

  //------------------------------------------------------------------//
  //                  Ethernet transmission module
  //------------------------------------------------------------------//

  else if ( Mod_ID_hex == rep_modID_ethernet_trans ) {

    if ( Message_Type_hex == rep_type_err ) {
      if ( datagram.at(2) == 0x0bad0001 ) {
	message_type = ethernet_trans_err_send_FIFO_full;
	*out_str = "Error: Ethernet Transmission data sending FIFO full \n";
	if ( m_dbg ) std::cout <<"error packet: ethernet transmission data sending fifo full" << std::endl;
        // outputDatagram(datagram);
        current_status = false;
      }
      else if ( datagram.at(2) == 0x0bad0002 ) {
	message_type = ethernet_trans_err_length_FIFO_full;
	*out_str = "Error: Ethernet Transmission data length FIFO full \n";
        if ( m_dbg ) std::cout <<"error packet: ethernet transmission data length fifo full" << std::endl;
        // outputDatagram(datagram);
        current_status = false;
      }
      else if ( datagram.at(2) == 0x0bad0003 ) {
	message_type = ethernet_trans_err_both_FIFO_full;
	*out_str = "Error: Ethernet Transmission data sending and length FIFO full \n";
        if ( m_dbg ) std::cout <<"error packet: ethernet transmission data sending and length fifo full" << std::endl;
        // outputDatagram(datagram);
        current_status = false;
      }
      else {
	message_type = ethernet_trans_err_undef;
	*out_str = "Error: unknown ethernet transmitter error \n";
        if ( m_dbg ) std::cout <<"error packet: ethernet transmission unknown error" << std::endl;
	// outputDatagram(datagram);
        current_status = false;
      }
    }
    else {
      message_type = ethernet_trans_err_undef;
      *out_str = "Error: unknown ethernet transmitter message \n";
      if ( m_dbg ) std::cout <<"error packet: ethernet transmission unknown message" << std::endl;
      // outputDatagram(datagram);
      current_status = false;
    }
  }

  //----------------------------------------------------------------//
  //                      SCA module
  //----------------------------------------------------------------//

  else if ( Mod_ID_hex == rep_modID_SCA ) {

    if ( datagram.size() < 3 ) {
      *out_str = "SCA Error: SCA packet too small, Unknown SCA Status  \n";
      if ( m_dbg ) std::cout <<"error packet: SCA reply packet too small, unknown SCA status" << std::endl;
      current_status = false;
    }

    //--------------------------------------------------//
    //                First Find Error
    //--------------------------------------------------//

    else if ( !(datagram.at( datagram.size()-3 ) == 0x0) ) {

      message_type = SCA_error;                                                                                                           

      uint32_t BAD             = ( datagram.at( datagram.size()-3 ) & 0x0FFF0000 ) >> 16;
      uint32_t timeoutBits     = ( datagram.at( datagram.size()-3 ) & 0x0000F000 ) >> 12;
      uint32_t SCA_err         = ( datagram.at( datagram.size()-3 ) & 0x00000F00 ) >> 8;
      uint32_t SCA_RX_err      = ( datagram.at( datagram.size()-3 ) & 0x000000FF );


      if ( timeoutBits ) {
	*out_str = "SCA Error: SCA timeout error \n";   
	if ( m_dbg ) std::cout <<"error packet: SCA timeout error" << std::endl;
      }
      else if (SCA_err == 0b01 ) {
        *out_str = "SCA Error: SCA error \n";          
	if ( m_dbg ) std::cout <<"error packet: SCA ASIC error" << std::endl;
      }
      else if (SCA_err == 0b10) {
	*out_str = "SCA Error: I2C error \n";
	if ( m_dbg ) std::cout <<"error packet: SCA I2C error" << std::endl;
      }
      else if (SCA_err == 0b11) {
        *out_str = "SCA Error: Unrecognized ASIC type \n"; 
	if ( m_dbg ) std::cout <<"error packet: SCA error, unrecognize ASIC type" << std::endl;
      }
      else if ( SCA_RX_err ) {
	*out_str = "SCA Error: Rx Error Message  \n";     
	if ( m_dbg ) std::cout <<"error packet: SCA Rx Error" << std::endl;
      }
      else {
	*out_str = "SCA Error: Unknown SCA Error  \n";
	std::cout << "SCA Error field " << datagram.at( datagram.size()-3 ) << std::endl;
	if ( m_dbg ) std::cout <<"error packet: Unknown SCA Error" << std::endl;
      }

      // if error return before decoding anything
      current_status = false;
    }
    
    //-------------------------------------------------//
    //          if no error decode packet
    //-------------------------------------------------//

    else {
      if ( sent_SCA_init ) {
	uint32_t FEB       = ( datagram.at(2) & 0xFFF00000 ) >> 20;
	uint32_t boardID   = ( datagram.at(2) & 0x000E0000 ) >> 17;
	uint32_t asicType  = ( datagram.at(2) & 0x00018000 ) >> 15;
	uint32_t asicID    = ( datagram.at(2) & 0x00007F80 ) >> 7;
	
	m_current_SCA_ID = ( datagram.at(3) & 0x00FFFFFF );
	sent_SCA_init = false;
	
	message_type = SCA_normal;
	*out_str = "SCA status normal \n";
	if ( m_dbg ) std::cout <<"info packet: SCA reply, SCA status normal" << std::endl;
	current_status = true;
      }
      else if ( sent_SCA_ADC ) {
	if ( m_dbg ) std::cout << " decoding ADC data " << std::endl;
	
	int size = 100;
	m_current_SCA_ADC.resize(0);
	
	if ( m_dbg ) std::cout << "datagram size " << datagram.size() << std::endl;
	
	if ( datagram.size() < size+4 ) {
	  message_type = SCA_error;
	  
	  *out_str = "SCA ADC reply is too short, not setting/decoding ADC \n";
	  if ( m_dbg ) std::cout << "SCA ADC reply is too short, not setting/decoding ADC, reply size " << datagram.size() << std::endl;
	  
	  sent_SCA_ADC = false;
	  current_status = false;
	}
	
	for ( int i=0; i < size; i++ ) {
	  m_current_SCA_ADC.push_back( ( datagram.at(4+i) & 0xffff ) ); 
	  m_current_SCA_ADC.push_back( ( datagram.at(4+i) & 0xffff0000 ) >> 16 );
	}
	
	sent_SCA_ADC = false;
	
	message_type = SCA_normal;
	*out_str = "SCA status normal \n";
	if ( m_dbg ) std::cout <<"info packet: SCA ADC query successful, SCA status normal" << std::endl;
	
	current_status = true;
      }
      else if ( sent_SCA_GPIO_readbk ) {
	std::cout << " decoding GPIO reply " << std::endl;
	
	if ( datagram.size() < 3 ) {
	  message_type = SCA_error;
	  *out_str = "SCA GPIO reply is too short, not decoding GPIO mask \n";
	  if ( m_dbg ) std::cout <<"error packet: SCA GPIO reply too short, unknown SCA status" << std::endl;
	  
	  sent_SCA_GPIO_readbk = false;
	  current_status = false;
	}
	else {
	  m_current_SCA_GPIO_mask = datagram.at(3);
	  
	  message_type = SCA_normal;
	  *out_str = "SCA status normal \n";
	  if ( m_dbg ) std::cout <<"info packet: SCA GPIO readback successful, SCA status normal" << std::endl;
	  
	  sent_SCA_GPIO_readbk = false;
	  current_status = true;
	}
      }
      
      else { // write GPIO reply, ASIC Config Reply
	message_type = SCA_normal;
	*out_str = "SCA status normal \n";
	if ( m_dbg ) std::cout <<"info packet: SCA reply, SCA status normal" << std::endl;
	
	current_status = true;
      }
    } // not error SCA packet

  }
 
  //--------------------------------------------------------------//
  //                        TTC module
  //--------------------------------------------------------------//

  else if ( Mod_ID_hex == rep_modID_TTC ) {
    if ( Message_Type_hex == rep_type_status ) {
      if ( Message_ID_hex == 0 ) {
        message_type = TTC_setting;
        *out_str = "decode TTC setting \n";
	if ( m_dbg ) std::cout <<"info packet: TTC Reply, setting TTC successful" << std::endl;

	decode_current_TTC_setting( datagram );
        current_status = true;
      }
      else if ( Message_ID_hex == Elink_status_hex ) {
	message_type = TTC_query_elink;
	*out_str = "decode query elink status \n";
        if ( m_dbg ) std::cout <<"info packet: TTC Reply, decoding query elink status" << std::endl;

	decode_query_elink( datagram.at(2) );
	current_status = true;
      }
      else if ( Message_ID_hex == EVID_status_hex ) {
        message_type = TTC_query_EVID;
        *out_str = "decode query EVID status \n";
        if ( m_dbg ) std::cout <<"info packet: TTC Reply, decoding query EVID status" << std::endl;

        decode_query_EVID( datagram.at(2) );
        current_status = true;
      }
      else if ( Message_ID_hex == TTC_status_hex ) {
        message_type = TTC_query_TTC;
        *out_str = "decode query TTC status \n";
        if ( m_dbg ) std::cout <<"info packet: TTC Reply, decoding query TTC status" << std::endl;

        decode_query_TTC_status( datagram.at(2) );
        current_status = true;
      }
      else if ( Message_ID_hex == TTC_firmware_hex ) {
        message_type = TTC_query_firmware;
        *out_str = "decode query firmware version \n";
        if ( m_dbg ) std::cout <<"info packet: TTC Reply, decoding query firmware version" << std::endl;

        decode_query_firmware_version( datagram.at(2) );
	current_status = true;
      }
      else {
	message_type = TTC_undef;
	*out_str = "Error: Unknown TTC response \n";
	if ( m_dbg ) std::cout <<"error packet: TTC Reply, unknown TTC response" << std::endl;
	
	// outputDatagram(datagram);
	current_status = false;

	//if ( datagram.size() >= 3 ) m_TTC_firmware_version = datagram.at(3);
      
      }
    }
    else {
      message_type = TTC_undef;
      *out_str = "Error: Unknown TTC response \n";
      if ( m_dbg ) std::cout <<"error packet: TTC Reply, unknown TTC response" << std::endl;
      // outputDatagram(datagram);
      current_status = false;
    }
  }
  else { // not any known module
    message_type = error_formatting;
    *out_str = "Error: could not find response type \n";
    current_status = false;
  }

  if ( listen ) {
    n_replies++;
    //  emit gotPacket();
    m_current_status.push_back( current_status );
  }

  return current_status;
}

//----------------------------------------------------------------------//
void Decoder::outputDatagram(std::vector<uint32_t> datagram)
{

  stringstream m_sx;

  m_sx << std::internal << std::setfill('0');

  for ( uint i=0; i < datagram.size(); i++ ) {
    m_sx << std::hex << std::setw(8) << datagram.at(i) << " " ;
  }

  if ( m_dbg ) std::cout << "Output datagram: " << m_sx.str() << std::endl;

}
//----------------------------------------------------------------------//
void Decoder::outputDatagram(uint32_t datagram)
{

  stringstream m_sx;

  m_sx << std::internal << std::setfill('0');

  m_sx << std::hex << std::setw(8) << datagram ;

  if ( m_dbg ) std::cout << " " << m_sx.str() << std::endl;

}

///////////////////////////////////////////////////////////////////////////////
bool Decoder::decode_vmm_event_data(uint32_t d0)
{
    bool ok = true;

    //----------------------------------------------------//
    //    Read data using bit mask and bit shifting       //
    //----------------------------------------------------//

    // tdo is first 8 bits;
    m_tdo.push_back( d0 & 0xff );

    // pdo is next 10 bits (after 8 bits)
    m_pdo.push_back( ( d0 & 0x3ff00 ) >> 8 );

    // channel ID is next 6 bits ( after 18 bits )
    m_chan.push_back( ( d0 & 0xFC0000 ) >> 18 );

    // vmm ID is next 3 bits ( after 24 bits )
    m_vmm_id.push_back( ( d0 & 0x7000000 ) >> 24 );

    // bcid relative is next 3 bits ( after 27 bits )
    m_bcid_rel.push_back( (d0 & 0x38000000 ) >> 27 );

    // flag is next 2 bits ( after 30 bits )
    m_flag.push_back( ( d0 & 0xc0000000 ) >> 30 );

    return ok;
}
//-----------------------------------------------------------------------//
bool Decoder::decode_vmm_raw_data(std::vector<uint32_t> datagram){

  if ( datagram.size() != 5 ) { //Example: FACE 0A9D 0BF2 0001 4200 A9DE DE00 0000 7770 0CE0 FFFF FFFF 
    std::cout << "warning: raw data event not of correct length" << std::endl;
    return false;
  }

  //-----------------------------------//
  //  Decode the 4x8 bit header first  //
  //-----------------------------------//

  //uint32_t header     = datagram.at(0); //UDP DGRAM HEADER (8b) + UDP PACEKT ID (8b)
  uint32_t gray_bcid = ( datagram.at(1) & 0x0fff0000)>>16;
  //cout<<"gray_bcid"<<gray_bcid<<endl;
  gray_bcid           = reverse_bits( gray_bcid, 12);
  //cout<<"reverse_gray_bcid"<<gray_bcid<<endl;
  m_raw_bcid          = grayToBinary( gray_bcid );
  //cout<<"real_bcid "<<m_raw_bcid<<endl;
  m_raw_l1id          = (datagram.at(1) & 0x0000FFFF);

  //-----------------------------------//
  //  Decode the 4x8 bit vmm hit data  //
  //-----------------------------------//

  m_raw_rel_bcid      = ( datagram.at(2) & 0x38000000 ) >> 27;
  m_raw_vmm_ID        = ( datagram.at(2) & 0x07000000 ) >> 24;
  
  uint32_t raw_chan_ID_tmp   = ( datagram.at(2) & 0xFC0000 ) >> 18 ;
  m_raw_chan_ID       = reverse_bits( raw_chan_ID_tmp, 6);

  uint32_t pdo_lsb_first  = ( datagram.at(2) & 0x3FF00 ) >>8;
  m_raw_pdo           = reverse_bits( pdo_lsb_first, 10 );

  m_raw_tdo           = reverse_bits( datagram.at(2) & 0xFF, 8 );

  m_raw_elink         = ( datagram.at(3) & 0x0000000f );

  //---------------------------//
  // 	 Forget Trailer Bits   //
  //------ --------------------//

  return true;

}
//-----------------------------------------------------------------------//
//                   Decode Data Packet from ROC ASIC
//-----------------------------------------------------------------------//
bool Decoder::decode_ROC_asic_packet( std::vector<uint32_t> datagram ) 
{

  bool all_ok = true;

  m_ROC_data_packet->clear();
  
  //-----------------------------------------------------------//

  if ( datagram.size() < 4 ) {
    std::cout << "error ROC asic packet length is too small" << std::endl;
    return false;
  }

  //12’h777 + 2’b0 + 2’b Packet_Byte_Error_Code + 8’h CE + 8’b ELINK CH #
  uint32_t elink_id = ( (datagram.at( datagram.size()-2 ) & 0x000000FF) );

  std::vector<uint32_t> ROC_datagram; // break it up into

  ROC_datagram.resize(0);

  for ( uint i=0; i<datagram.size() - 4; i++ ) {
    ROC_datagram.push_back( datagram.at(i+2) );

  }

  //----------------------------------------------------------//

  uint32_t SOP;
  uint32_t ROCID;

  bool withTDC;

  uint32_t orbit;
  uint32_t BCID;
  uint32_t L1ID;

  uint32_t EOP;

  uint32_t nHits;

  std::vector<uint32_t> hit_isDummy;
  std::vector<uint32_t> hit_parity;
  std::vector<uint32_t> hit_neighbour;
  std::vector<uint32_t> hit_rel_BCID;
  std::vector<uint32_t> hit_vmm_id;
  std::vector<uint32_t> hit_chan_id;
  std::vector<uint32_t> hit_ADC;
  std::vector<uint32_t> hit_TDC;

  //---------------------------------------------------------//

  //-------------------------------------//
  //      Null Packet has 32 bits
  //-------------------------------------//

  cout<<"datagram size: "<<ROC_datagram.size()<<endl;

  if ( ROC_datagram.size() == 1 ) {

    //----------------------------------------------------------------------------------//
    //                       Null Event Packet Format
    //   8 bit EOP  | 8 bit L1ID  |  0b0  0b1 NullEventTag  6bit ROCID |  8 bit SOP | 
    //   SOP seems to be MSB though...  EOP is LSB...
    //----------------------------------------------------------------------------------//

    if ( (ROC_datagram.at(0) & 0x00400000) ) {
      m_ROC_data_packet->set_isNull( true );
    
      SOP   = ( ( ROC_datagram.at(0) & 0xFF000000 ) >> 24 );
      ROCID = ( ( ROC_datagram.at(0) & 0x003F0000 ) >> 16 );
      L1ID  = ( ( ROC_datagram.at(0) & 0x0000FF00 ) >> 8 );
      EOP   = ( ( ROC_datagram.at(0) & 0x000000FF ) );

      m_ROC_data_packet->set_SOP( SOP );
      m_ROC_data_packet->set_ROCID( ROCID );
      m_ROC_data_packet->set_L1ID( L1ID );
      m_ROC_data_packet->set_EOP( EOP );

      return true;

    }
    else {
      std::cout << "Error:  ROC data packet is only 1 word long but it is not a null backpack.  Aborting" << std::endl;
      return false;
    }
    
  }

  //-------------------------------------//
  //            Data packet              //
  //-------------------------------------//

  //-------------------------------------//
  //         Decode Hit Header           //
  //-------------------------------------//

  //MSB first
  SOP = ( (ROC_datagram.at(0) & 0xFF000000) >> 24 );

  if ( !( (ROC_datagram.at(0) & 0x00800000) >> 23 ) ) withTDC = true;
  else withTDC = false;

  if ( (ROC_datagram.at(0) & 0x00400000) >> 22 ) {
    std::cout << "ERROR: WTF ROC packet is not supposed to have null packet bit as 1 for data packets" << std::endl;
    return false;
  }

  orbit = ( ( ROC_datagram.at(0) & 0x00300000) >> 20);
  BCID  = ( ( ROC_datagram.at(0) & 0x000FFF00) >> 8);
  L1ID  = ( ((ROC_datagram.at(0) & 0x000000FF) << 8) +
            ((ROC_datagram.at(1) & 0xFF000000) >> 24) );  

  //-------------------------------------//
  //          Loop over Hits             //
  //-------------------------------------//

  uint32_t hitLength;
  uint32_t totalLength;

  if ( withTDC ) hitLength = 8; // 8 4 bit sections per hit
  else           hitLength = 6; // 6 4 bit sections per hit

  totalLength = ROC_datagram.size() * 8; // 32 bits per ROC size so 8*4 bits

  nHits = (totalLength - 10 - 10) / hitLength; // totalLength - 10 bytes for hit header - 10 bytes for end of packet trailer

  //std::cout << "total length " << totalLength << std::endl;
  //std::cout << "nHits        " << nHits << std::endl;

  for ( uint iHit=0; iHit < nHits; iHit++ ) {

    int position = iHit * hitLength + 10; // 10 byte for header
    
    int i_datagram = position / 8; // which ith datagram value;
    int i_startbyte = position % 8; // position of starting byte;

    if ( i_datagram >= ROC_datagram.size() ) {
      std::cout << "Error: WTF trying to decode hits outside of ROC datagram size" << std::endl;
      outputDatagram( ROC_datagram );
      return false;
    }

    uint32_t i_parity   = ( ROC_datagram.at(i_datagram) & (0x80000000 >> (i_startbyte*4) ) ) >> (24-(i_startbyte*4)+7);
    uint32_t i_neighbour= ( ROC_datagram.at(i_datagram) & (0x40000000 >> (i_startbyte*4) ) ) >> (24-(i_startbyte*4)+6); 
    uint32_t i_rel_bcid = ( ROC_datagram.at(i_datagram) & (0x38000000 >> (i_startbyte*4) ) ) >> (24-(i_startbyte*4)+3); 
    uint32_t i_vmm_id   = ( ROC_datagram.at(i_datagram) & (0x07000000 >> (i_startbyte*4) ) ) >> (24-(i_startbyte*4));

    i_startbyte+=2;
    
    // if the byte location is over 8 bytes, then we should be in the next 32 datagram and start again at 0th byte
    if ( i_startbyte >= 8 ) { i_datagram++; i_startbyte = 0; }  
    if ( i_datagram >= ROC_datagram.size() ) {
      std::cout << "Error: WTF trying to decode hits outside of ROC datagram size" << std::endl;
      outputDatagram( ROC_datagram );
      return false;
    }

    uint32_t i_chan_id  = ( ROC_datagram.at(i_datagram) & (0xFC000000 >> (i_startbyte*4) ) ) >> (24-(i_startbyte*4)+2);
    uint32_t i_adc_2b   = ( ROC_datagram.at(i_datagram) & (0x03000000 >> (i_startbyte*4) ) ) >> (24-(i_startbyte*4));
    
    i_startbyte+=2;

    if ( i_startbyte >= 8 ) { i_datagram++; i_startbyte = 0; }
    if ( i_datagram >= ROC_datagram.size() ) {
      std::cout << "Error: WTF trying to decode hits outside of ROC datagram size" << std::endl;
      outputDatagram( ROC_datagram );
      return false;
    }

    uint32_t i_adc_8b   = ( ROC_datagram.at(i_datagram) & (0xFF000000 >> (i_startbyte*4) ) ) >> (24-i_startbyte*4);

    uint32_t i_adc = ( i_adc_2b << 8 ) + i_adc_8b;

    i_startbyte+=2;
    if ( i_startbyte >= 8 ) { i_datagram++; i_startbyte = 0; }
    if ( i_datagram >= ROC_datagram.size() ) {
      std::cout << "Error: WTF trying to decode hits outside of ROC datagram size" << std::endl;
      outputDatagram( ROC_datagram );
      return false;
    }

    uint32_t i_tdc = 0;

    if ( withTDC ) i_tdc = ( ROC_datagram.at(i_datagram) & (0xFF000000 >> (i_startbyte*4) ) ) >> (24-i_startbyte*4);

    bool i_isDummy = false;

    if ( i_rel_bcid == 0 && i_chan_id == 0 && i_adc == 0x3FF && i_tdc == 0 ) i_isDummy = true;

    hit_isDummy.push_back( i_isDummy );
    hit_neighbour.push_back( i_neighbour );
    hit_parity.push_back( i_parity );
    hit_rel_BCID.push_back( i_rel_bcid );
    hit_vmm_id.push_back( i_vmm_id );
    hit_chan_id.push_back( i_chan_id );
    hit_ADC.push_back( i_adc );
    hit_TDC.push_back( i_tdc );

  }

  //-------------------------------------//
  //           End of Packet             //
  //-------------------------------------//

  // last 10 bytes
  int position = nHits * hitLength + 10;
  int i_datagram = position / 8; // which ith datagram value;
  int i_startbyte = position % 8; // position of starting byte;

  if ( i_datagram >= ROC_datagram.size() ) {
    std::cout << "Error: WTF trying to decode EOP outside of ROC datagram size" << std::endl;
    outputDatagram( ROC_datagram );
    return false;
  }

  uint32_t error_flag                =   ( ( ROC_datagram.at( i_datagram ) & ( 0x80000000 >> (i_startbyte*4) ) ) >> (24-(i_startbyte*4)+7) );
  uint32_t timeout_flag              =   ( ( ROC_datagram.at( i_datagram ) & ( 0x40000000 >> (i_startbyte*4) ) ) >> (24-(i_startbyte*4)+6) );
  uint32_t VMM_missing_data_flags_6b =   ( ( ROC_datagram.at( i_datagram ) & ( 0x3F000000 >> (i_startbyte*4) ) ) >> (24-(i_startbyte*4)) );

  i_startbyte+=2;
  if ( i_startbyte >= 8 ) { i_datagram++; i_startbyte = 0; }
  if ( i_datagram >= ROC_datagram.size() ) {
    std::cout << "Error: WTF trying to decode EOP outside of ROC datagram size" << std::endl;
    outputDatagram( ROC_datagram );
    return false;
  }

  uint32_t VMM_missing_data_flags_2b =   ( ( ROC_datagram.at( i_datagram ) & ( 0xC0000000 >> (i_startbyte*4) ) ) >> (24-(i_startbyte*4)+6) );
  uint32_t L0ID                      =   ( ( ROC_datagram.at( i_datagram ) & ( 0x3C000000 >> (i_startbyte*4) ) ) >> (24-(i_startbyte*4)+2) ); // is this Gray counter?
  uint32_t length_2b                 =   ( ( ROC_datagram.at( i_datagram ) & ( 0x03000000 >> (i_startbyte*4) ) ) >> (24-(i_startbyte*4)) );

  uint32_t VMM_missing_data_flags = ( VMM_missing_data_flags_6b << 2 ) + ( VMM_missing_data_flags_2b );

  i_startbyte+=2;
  if ( i_startbyte >= 8 ) { i_datagram++; i_startbyte = 0; }
  if ( i_datagram >= ROC_datagram.size() ) {
    std::cout << "Error: WTF trying to decode EOP outside of ROC datagram size" << std::endl;
    outputDatagram( ROC_datagram );
    return false;
  }

  uint32_t length_8b                 =   ( ( ROC_datagram.at( i_datagram ) & ( 0xFF000000 >> (i_startbyte*4) ) ) >> (24-i_startbyte*4) );
  
  uint32_t length = ( length_2b << 8 ) + length_8b;

  i_startbyte+=2;
  if ( i_startbyte >= 8 ) { i_datagram++; i_startbyte = 0; }
  if ( i_datagram >= ROC_datagram.size() ) {
    std::cout << "Error: WTF trying to decode EOP outside of ROC datagram size" << std::endl;
    outputDatagram( ROC_datagram );
    return false;
  }

  uint32_t checksum                  =   ( ( ROC_datagram.at( i_datagram ) & ( 0xFF000000 >> (i_startbyte*4) ) ) >> (24-i_startbyte*4) );

  i_startbyte+=2;
  if ( i_startbyte >= 8 ) { i_datagram++; i_startbyte = 0; }
  if ( i_datagram >= ROC_datagram.size() ) {
    std::cout << "Error: WTF trying to decode EOP outside of ROC datagram size" << std::endl;
    outputDatagram( ROC_datagram );
    return false;
  }

  EOP                                =   ( ( ROC_datagram.at( i_datagram ) & ( 0xFF000000 >> (i_startbyte*4) ) ) >> (24-i_startbyte*4) );

  m_ROC_data_packet->set_isNull( false );

  m_ROC_data_packet->set_SOP( SOP );
  m_ROC_data_packet->set_withTDC( withTDC );
  m_ROC_data_packet->set_orbit( orbit );
  m_ROC_data_packet->set_BCID( BCID );
  m_ROC_data_packet->set_L1ID( L1ID );  
  m_ROC_data_packet->set_elink_id( elink_id );

  m_ROC_data_packet->set_nhits( nHits );
  m_ROC_data_packet->set_hit_isDummy( hit_isDummy );
  m_ROC_data_packet->set_hit_parity( hit_parity );
  m_ROC_data_packet->set_hit_neighbour( hit_neighbour );
  m_ROC_data_packet->set_hit_rel_BCID( hit_rel_BCID );
  m_ROC_data_packet->set_hit_vmm_id( hit_vmm_id );
  m_ROC_data_packet->set_hit_chan_id( hit_chan_id );
  m_ROC_data_packet->set_hit_ADC( hit_ADC );
  m_ROC_data_packet->set_hit_TDC( hit_TDC);

  m_ROC_data_packet->set_error_flag( error_flag );
  m_ROC_data_packet->set_timeout_flag( timeout_flag );
  m_ROC_data_packet->set_VMM_missing_data_flags( VMM_missing_data_flags );
  m_ROC_data_packet->set_L0ID( L0ID );
  m_ROC_data_packet->set_length( length );
  m_ROC_data_packet->set_checksum( checksum );
  m_ROC_data_packet->set_EOP( EOP );

  return true;

}

//-----------------------------------------------------------------------//
bool Decoder::decode_query_elink(uint32_t d0)
{
  bool ok = true;

  //----------------------------------------------------//                        
  //    Read data using bit mask and bit shifting       //                        
  //----------------------------------------------------//                        

  m_locked_elink.resize(0);

  uint32_t bit_mask = 0b1;

  for ( int i=0; i<8; i++ ) {
    std::vector<bool> m_locked_elink_i;
    m_locked_elink_i.resize(0);

    for ( int j=0; j<4; j++ ) {

      if ( d0 & bit_mask ) m_locked_elink_i.push_back(true);
      else                 m_locked_elink_i.push_back(false);
      bit_mask = (bit_mask << 1); // shift bit mask left by 1 (eg: 0001 to 0010)

    }
    m_locked_elink.push_back( m_locked_elink_i );

  }
  return ok;
}

//-----------------------------------------------------------------------//
bool Decoder::decode_query_EVID(uint32_t d0)
{
  bool ok = true;

  //----------------------------------------------------//                 
  //    Read data using bit mask and bit shifting       //                 
  //----------------------------------------------------//                 

  m_current_EVID = ( d0 & 0xffff ) ;

  return ok;
}


//-----------------------------------------------------------------------// 
bool Decoder::decode_query_TTC_status(uint32_t d0)
{

  bool ok = true;

  m_current_TTC_status = ( d0 & 0xffff );

  return ok;
}

bool Decoder::decode_query_firmware_version(uint32_t d0)
{
  bool ok = true;

  //----------------------------------------------------//
  //    Read data using bit mask and bit shifting       //
  //----------------------------------------------------//
  m_current_firmware_version = ( d0 ) ;

  return ok;
}


//-----------------------------------------------------------------------//
void Decoder::decode_current_TTC_setting(std::vector<uint32_t> datagram ) {

  m_TTC_reg_address.resize(0);
  m_TTC_reg_value.resize(0);
  for ( uint i=2; i<datagram.size()-1; i=i+2 ) {
    m_TTC_reg_address.push_back( datagram.at(i) );
    m_TTC_reg_value.push_back( datagram.at(i+1) );
  }

}

//----------------------------------------------------------------------//



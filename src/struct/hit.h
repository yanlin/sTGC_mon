#ifndef HIT_H
#define HIT_H

#include <vector>
#include <string>
#include <cstdint>

#include <stdint.h>
#include <TMath.h>


class hit {

    public :
        hit();
        virtual ~hit(){};

        /////////////////////////////////////////
        //      
        /////////////////////////////////////////
	int EVID;
	uint32_t udp_id;
	uint32_t bcid;
	uint32_t neighbour;
	uint32_t parity;
	uint32_t bcid_rel;
	uint32_t elink_id;
	uint32_t vmm_id;
	uint32_t chan_id;
	uint32_t tdo;
	uint32_t pdo;

        void clear();
        bool ok; // loading went ok
};

#endif
